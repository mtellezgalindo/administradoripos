<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatTableInstancia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('instancia', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('instancia_id');
            $table->integer('user_id');
            $table->string('factura_publico_id');
            $table->timestamps();
        });

        //factura_publico_id
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('instancia');
    }
}
