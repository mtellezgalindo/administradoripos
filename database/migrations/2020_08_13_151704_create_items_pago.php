<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsPago extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items_pago', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('precio_de_origen');
            $table->integer('segundo_precio_origen');
            $table->string('elemento_factura_id');
            $table->string('factura_id');
            $table->string('factura_publico_id');
            $table->string('elemento_id');
            $table->string('elemento_tipo');
            $table->string('elemento_titulo');
            $table->string('elemento_descripcion');
            $table->string('cantidad');
            $table->string('precio');
            $table->string('primer_periodo');
            $table->string('tipo_moneda');
            $table->string('plan_de_pago_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items_pago');
    }
}
