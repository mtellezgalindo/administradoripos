<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableInstanciaDetalle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instancia_detalle', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('instancia_id');
            $table->string('url',115);
            $table->string('disponibilidad_zona');
            $table->string('estatus_instancia');
            $table->string('estatus_sistema');
            $table->string('dns_name');
            $table->string('imagen_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instancia_detalle');
    }
}
