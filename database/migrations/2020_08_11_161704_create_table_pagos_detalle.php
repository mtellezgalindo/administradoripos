<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTablePagosDetalle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pagos_detalle', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('factura_id');
            $table->integer('user_id');
            $table->string('metodo_de_pago');
            $table->string('tipo_moneda');
            $table->string('subtotal');
            $table->string('periodo');
            $table->string('estatus');
            $table->string('confirmacion');
            $table->string('public_id');
            $table->string('factura_identificacion');
            $table->string('fecha_adicion');
            $table->string('fecha_de_comienzo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pagos_detalle');
    }
}
