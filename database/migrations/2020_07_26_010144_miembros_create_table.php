<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MiembrosCreateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('miembros', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('email');
            $table->string('nombre');
            $table->string('apellidos')->nullable();
            $table->string('calle')->nullable();
            $table->string('calle_2')->nullable();
            $table->string('ciudad')->nullable();
            $table->string('estado')->nullable();
            $table->string('codigo_postal')->nullable();
            $table->string('telefono')->nullable();
            $table->dateTime('fecha_de_ingreso');
            $table->boolean('status');
            $table->boolean('aprovado');
            $table->string('colonia')->nullable();
            $table->string('delegacion')->nullable();
            $table->string('establecimiento')->nullable();
            $table->text('apellido_paterno')->nullable();
            $table->text('apellido_materno')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('miembros');
    }
}
