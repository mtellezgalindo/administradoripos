<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PagosMiembroCreateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('pagos_miembros', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('id_pago_factura');
            $table->string('factuar_id');
            $table->string('factura_id_publico');
            $table->string('metodo_de_pago');
            $table->string('id_recibo');
            $table->string('id_transaccion');
            $table->dateTime('fecha_transaccion');
            $table->string('tipo_moneda');
            $table->string('cantidad');
            $table->string('discuento');
            $table->string('impuesto');
            $table->string('envio');
            $table->string('id_display_factura');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('pagos_miembros');
    }
}
