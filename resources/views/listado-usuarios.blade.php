@extends('layouts.usuarios')

@section('content')
    <div class="layout-px-spacing">

        <div class="row layout-top-spacing" id="cancel-row">

            <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                <div class="widget-content widget-content-area br-6">
                    <div class="table-responsive mb-4 mt-4">
                        <table id="html5-extension" class="table table-hover non-hover" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Email</th>
                                    <th>Telefono</th>
                                    <th>Fecha de ingreso</th>

                                    <th>Acción</th>
                                </tr>
                            </thead>
                            <tbody>
                       @php
                         //  dd($usuarios);
                       @endphp
                            @foreach( $usuarios as $usuario)

                                <tr>
                                    <td>{!! $usuario['nombre']  !!}</td>
                                    <td>{!! $usuario['email'] !!}</td>
                                    <td>{!! $usuario['telefono'] !!}</td>
                                    <td>{!! $usuario['fecha_de_ingreso'] !!}</td>

                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-dark btn-sm">Abrir</button>
                                            <button type="button" class="btn btn-dark btn-sm dropdown-toggle dropdown-toggle-split" id="dropdownMenuReference1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-reference="parent">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down"><polyline points="6 9 12 15 18 9"></polyline></svg>
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuReference1">
                                                <a class="dropdown-item" href="{{route('detalle-usuario',$usuario['user_id'])}}">Detalle Usuarios</a>
                                                <a class="dropdown-item" href="#">Acción</a>
                                                <a class="dropdown-item" href="#">Acción</a>
                                                <div class="dropdown-divider"></div>
                                                <a class="dropdown-item" href="#">Acción</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>

    </div>
@endsection
