@extends('layouts.usuarios')
@section('content')
    @php
    $usuarioId = '';
    $idFacturaPublica = '';
    @endphp
    <div class="layout-px-spacing">
        <div class="row layout-spacing">
            
            <div class="col-xl-4 col-lg-6 col-md-5 col-sm-12 layout-top-spacing">
                <div class="user-profile layout-spacing">
                    <div class="widget-content widget-content-area">
                        <div class="d-flex justify-content-between">
                            <h3 class="">Información</h3>
                        </div>
                        @foreach($detalles['info_personal'] as $usuarios)
                            @php
                                $usuarioId = $usuarios['user_id'];
                            @endphp
                        <div class="text-center user-info">
                            <img src="{{ asset('assets/img/90x90.jpg')}}" alt="avatar">
                            <p class="">{{$usuarios['nombre']}} {{$usuarios['apellidos']}}</p>
                        </div>
                        <div class="user-info-list">
                            <div class="">
                                <ul class="contacts-block list-unstyled">
                                    <li class="contacts-block__item">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar"><rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect><line x1="16" y1="2" x2="16" y2="6"></line><line x1="8" y1="2" x2="8" y2="6"></line><line x1="3" y1="10" x2="21" y2="10"></line></svg>{{$usuarios['fecha_de_ingreso']}}
                                    </li>
                                    <li class="contacts-block__item">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-map-pin"><path d="M21 10c0 7-9 13-9 13s-9-6-9-13a9 9 0 0 1 18 0z"></path><circle cx="12" cy="10" r="3"></circle></svg>{{$usuarios['estado']}}
                                    </li>
                                    <li class="contacts-block__item">
                                        <a href="mailto:example@mail.com"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-mail"><path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path><polyline points="22,6 12,13 2,6"></polyline></svg>{{$usuarios['email']}}</a>
                                    </li>
                                    <li class="contacts-block__item">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-phone"><path d="M22 16.92v3a2 2 0 0 1-2.18 2 19.79 19.79 0 0 1-8.63-3.07 19.5 19.5 0 0 1-6-6 19.79 19.79 0 0 1-3.07-8.67A2 2 0 0 1 4.11 2h3a2 2 0 0 1 2 1.72 12.84 12.84 0 0 0 .7 2.81 2 2 0 0 1-.45 2.11L8.09 9.91a16 16 0 0 0 6 6l1.27-1.27a2 2 0 0 1 2.11-.45 12.84 12.84 0 0 0 2.81.7A2 2 0 0 1 22 16.92z"></path></svg> {{$usuarios['telefono']}}
                                    </li>
                                </ul>
                            </div>
                        </div>
                        @endforeach

                    </div>
                </div>
            </div>

            <div class="col-xl-8 col-lg-6 col-md-7 col-sm-12 layout-top-spacing">
                <div class="skills layout-spacing ">
                    <div class="statbox widget box box-shadow">
                        <div id="accordionIcons" class="widget-header">
                            <div class="row">
                                <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                    <h4>Detalle facturas</h4>
                                </div>
                            </div>
                        </div>
                        <div class="widget-content widget-content-area">
                            @if(!empty($detalles['factura']))
                                <div id="iconsAccordion" class="accordion-icons">

                                    @foreach($detalles['factura'] as $facturas)

                                        @foreach($facturas['itemFactura'] as $itemFac)

                                            <div class="card">

                                                <div class="card-header" id="headingOne3">
                                                    <section class="mb-0 mt-0">
                                                        <div role="menu" class="collapsed" data-toggle="collapse" data-target="#iconAccordion{{$itemFac['id']}}" aria-expanded="true" aria-controls="iconAccordion{{$itemFac['id']}}">
                                                            {{$itemFac['factura_publico_id']}}  -   {{$itemFac['elemento_descripcion']}}  <div class="icons"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down"><polyline points="6 9 12 15 18 9"></polyline></svg></div>
                                                        </div>
                                                    </section>
                                                </div>
                                                <div id="iconAccordion{{$itemFac['id']}}" class="collapse" aria-labelledby="headingOne3" data-parent="#iconsAccordion">
                                                    <div class="card-body">
                                                        <div class="widget-header">
                                                            <div class="row">
                                                                <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                                                    <h4>Pagos realizado</h4>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="widget-content widget-content-area">
                                                            <div class="table-responsive">
                                                                <table class="table table-bordered table-hover mb-4">
                                                                    <thead>
                                                                    <tr>
                                                                        <th>Factura Id</th>
                                                                        <th>Metodo de pago</th>
                                                                        <th>Recibo</th>
                                                                        <th>Fecha de pago</th>
                                                                        <th>Cantidad</th>
                                                                        <th class="text-center">Status</th>
                                                                        <th></th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    @foreach($facturas['facturaPagada'] as $item)
                                                                        @php
                                                                            $idFacturaPublica = $item['factura_id_publico'];
                                                                        @endphp
                                                                        <tr>
                                                                            <td>{{$item['factura_id_publico']}}</td>
                                                                            <td>{{$item['metodo_de_pago']}}</td>
                                                                            <td>{{$item['id_recibo']}}</td>
                                                                            <td>{{$item['fecha_transaccion']}}</td>
                                                                            <td>{{$item['cantidad']}}</td>
                                                                            <td class="text-center"><span class="text-success">Complete</span></td>
                                                                        </tr>
                                                                    @endforeach
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card-body">
                                                        <div class="widget-header">
                                                            <div class="row">
                                                                <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                                                    <h4>Datos  capsula</h4>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="analytics col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
                                                            <div class="widget widget-activity-two">

                                                                <div class="widget-content">
                                                                    
                                                                    <div class="mt-container mx-auto">
                                                                        <div class="timeline-line">
                                                                    
                                                                            <div class="item-timeline timeline-new">
                                                                                <div class="t-dot" data-original-title="" title="">
                                                                                </div>
                                                                                <div class="t-text">
                                                                                    <p>Url de capsula <span>{{$facturas['instanciaDetalle']['url']}}</span></p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="item-timeline timeline-new">
                                                                                <div class="t-dot" data-original-title="" title="">
                                                                                </div>
                                                                                <div class="t-text">
                                                                                    <p>Zona <span>{{$facturas['instanciaDetalle']['disponibilidad_zona']}}</span></p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="item-timeline timeline-new">
                                                                                <div class="t-dot" data-original-title="" title="">
                                                                                </div>
                                                                                <div class="t-text">
                                                                                    <p>Estatus <span>{{$facturas['instanciaDetalle']['estatus_instancia']}}</span></p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="item-timeline timeline-new">
                                                                                <div class="t-dot" data-original-title="" title="">
                                                                                </div>
                                                                                <div class="t-text">
                                                                                    <p>DNS <span>{{$facturas['instanciaDetalle']['dns_name']}}</span></p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="item-timeline timeline-new">
                                                                                <div class="t-dot" data-original-title="" title="">
                                                                                </div>
                                                                                <div class="t-text">
                                                                                    <p>Id Imagen <span>{{$facturas['instanciaDetalle']['imagen_id']}}</span></p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="item-timeline timeline-new">
                                                                                <div class="t-dot" data-original-title="" title="">
                                                                                </div>
                                                                                <div class="t-text">
                                                                                    <p>Fecha creación <span>{{$facturas['instanciaDetalle']['created_at']}}</span></p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    @if($facturas['instancia'])
                                                        <div class="card-body">
                                                            <div class="widget-header">
                                                                <div class="row">
                                                                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                                                        <h4>Acciones en capsula</h4>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="widget-content widget-content-area">

                                                                <div class="text-center">
                                                                    <a class="btn btn-success mb-2c ml-5" href="{{route('crear-capsula',[$usuarioId, $idFacturaPublica], true)}}">Crear capsula</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @else
                                                        <div class="card-body">
                                                            <div class="widget-header">
                                                                <div class="row">
                                                                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                                                        <h4>Estatus capsula</h4>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="widget-content widget-content-area">
                                                                <div class="text-center">
                                                                   
                                                                    <a class="btn btn-warning mb-2c ml-5" href="#">Detener capsula</a>
                                                                    <a class="btn btn-danger mb-2c ml-5" href="#">Borrar capsula</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                </div>
                                        </div>
                                        @endforeach
                                    @endforeach
                                </div>
                            @else
                                <div class="card">
                                    <div class="card-body" >
                                        <p>El usuario no ha realizado pago aún</p>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
