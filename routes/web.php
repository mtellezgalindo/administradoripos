<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/listado-usuarios', 'MiembroController@getUsers')->name('listado-usuarios');
Route::get('/listado-facturas', 'FacturasController@getFacturas')->name('listado-facturas');
Route::get('/listado-facturas-pagadas','FacturasController@getFacturasPagadas')->name('listado-facturas-pagadas');
Route::get('/detalle-usuario/{id}', 'MiembroController@getDetalleUsuario')->name('detalle-usuario');
Route::get('/crear-capsula/{id}/{idFactura}','AwsController@capsulaCreate')->name('crear-capsula');

