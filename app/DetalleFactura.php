<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleFactura extends Model
{
    //
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'detalle_pago';
    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $id = 'id';
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    protected $fillable = ['id','factura_id','user_id','metodo_de_pago','tipo_moneda','subtotal','periodo','estatus','confirmacion','public_id',
        'factura_identificacion','fecha_adicion','fecha_de_comienzo','fecha_refacturacion','create_at','updated_at'];

}
