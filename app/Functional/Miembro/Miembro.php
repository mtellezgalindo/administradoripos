<?php


namespace App\Functional\Miembro;


use App\Functional\Miembro\Service\GetMiembro;

class Miembro implements IMiembro
{
    use GetMiembro;
}
