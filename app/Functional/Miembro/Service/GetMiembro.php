<?php


namespace App\Functional\Miembro\Service;
use App\DetalleFactura;
use App\Facturacion;
use App\Instancia;
use App\InstanciaDetalle;
use App\ItemFactura;
use App\Miembro;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

trait GetMiembro
{
    public function getallMembers(){
        DB::table('miembros')->truncate();

        $member = Http::get('https://sandbox.iposbyivan.com/customer/api/users?_key=ZqjkoHHNewTZdSyAjbOf');

        $this->saveMembers($member->json());

        $i = 0;
        $usuarios = Miembro::orderBy('fecha_de_ingreso','DESC')->get();

        foreach ($usuarios as $key => $value){
            $miembros[$i]['id'] = $value->id;
            $miembros[$i]['user_id'] = $value->user_id;
            $miembros[$i]['nombre'] = $value->nombre ." ". $value->apellidos;
            $miembros[$i]['email'] = $value->email;
            $miembros[$i]['telefono'] = $value->telefono ;
            $miembros[$i]['fecha_de_ingreso'] = $value->fecha_de_ingreso ;


            $factura = DetalleFactura::where('user_id', $value->user_id)->get();

            foreach ($factura as $k => $v){

                $miembros[$i]['subtotal'] = $v->subtotal;
                $miembros[$i]['factura'] = $v->factura_identificacion;
                $miembros[$i]['terminos'] = $v->terminos;
                $miembros[$i]['status'] = $v->estatus;

            }
            $i++;
        }


        return $miembros;


    }

    public function getAllInvoice(){
        $facturasApi = Http::get("https://sandbox.iposbyivan.com/customer/api/invoices?_key=ZqjkoHHNewTZdSyAjbOf&_count=100");

        try {
            DB::table('detalle_pago')->truncate();
            DB::table('items_pago')->truncate();
            DB::table('pagos_miembros')->truncate();
           $this->saveFacturas($facturasApi->json());
            //$usuarios = Miembro::get();
            //return $usuarios;
        }catch (\Exception $exception){
            return  $exception->getMessage();
        }
    }

    public function saveFacturas($facturas){
        $idUsuario = "";

        foreach (array_slice($facturas,1)as $key => $value){
            $nuevaFactura = new DetalleFactura();
            $nuevaFactura->factura_id = $value['invoice_id'];
            $nuevaFactura->user_id = $value['user_id'];
            $idUsuario =$value['user_id'];
            $nuevaFactura->metodo_de_pago = $value['paysys_id'];
            $nuevaFactura->tipo_moneda = $value['currency'];
            $nuevaFactura->subtotal = $value['first_subtotal'];
            $nuevaFactura->periodo = $value['first_period'];
            $nuevaFactura->estatus = $value['status'];
            $nuevaFactura->confirmacion = $value['is_confirmed'];
            $nuevaFactura->public_id = $value['public_id'];
            $nuevaFactura->factura_identificacion = $value['invoice_key'];
            $nuevaFactura->fecha_adicion = $value['tm_added'];
            $nuevaFactura->fecha_de_comienzo = $value['tm_started'];
            $nuevaFactura->fecha_refacturacion = $value['rebill_date'];
            $nuevaFactura->terminos = $value['terms'];
            $nuevaFactura->save();
            if (isset($value['nested']['invoice-items'])){
               foreach ($value['nested']['invoice-items'] as $item){
                   $itemInvoice = new ItemFactura();
                   $itemInvoice->user_id = $idUsuario;
                   $itemInvoice->precio_de_origen = $item['orig_first_price'];
                   $itemInvoice->segundo_precio_origen = $item['orig_second_price'];
                   $itemInvoice->elemento_factura_id = $item['invoice_item_id'];
                   $itemInvoice->factura_id = $item['invoice_id'];
                   $itemInvoice->factura_publico_id = $item['invoice_public_id'];
                   $itemInvoice->elemento_id = $item['item_id'];
                   $itemInvoice->elemento_tipo = $item['item_type'];
                   $itemInvoice->elemento_titulo = $item['item_title'];
                   $itemInvoice->elemento_descripcion = $item['item_description'];
                   $itemInvoice->cantidad = $item['qty'];
                   $itemInvoice->precio = $item['first_price'];
                   $itemInvoice->primer_periodo = $item['first_period'];
                   $itemInvoice->tipo_moneda = $item['currency'];
                   $itemInvoice->plan_de_pago_id = $item['billing_plan_id'];
                   $itemInvoice->save();

               }
            }
            if (isset($value['nested']['invoice-payments'])){

                foreach ($value['nested']['invoice-payments'] as $itemFac){
                    $nuevaFacturaPagada = new Facturacion();
                    $nuevaFacturaPagada->user_id = $itemFac['user_id'];
                    $nuevaFacturaPagada->id_pago_factura = $itemFac['invoice_payment_id'];
                    $nuevaFacturaPagada->factuar_id = $itemFac['invoice_id'];
                    $nuevaFacturaPagada->factura_id_publico = $itemFac['invoice_public_id'];
                    $nuevaFacturaPagada->metodo_de_pago = $itemFac['paysys_id'];
                    $nuevaFacturaPagada->id_recibo = $itemFac['receipt_id'];
                    $nuevaFacturaPagada->id_transaccion = $itemFac['transaction_id'];
                    $nuevaFacturaPagada->fecha_transaccion = $itemFac['dattm'];
                    $nuevaFacturaPagada->tipo_moneda = $itemFac['currency'];
                    $nuevaFacturaPagada->cantidad = $itemFac['amount'];
                    $nuevaFacturaPagada->discuento = $itemFac['discount'];
                    $nuevaFacturaPagada->impuesto = $itemFac['tax'];
                    $nuevaFacturaPagada->envio = $itemFac['shipping'];
                    $nuevaFacturaPagada->id_display_factura = $itemFac['display_invoice_id'];
                    $nuevaFacturaPagada->save();
                }
            }



        }
    }

    public function saveMembers($miembrosList){

        foreach (array_slice($miembrosList,1) as $key => $value){
            $nuevoMiembro = new Miembro();
            $nuevoMiembro->user_id = $value['user_id'];
            $nuevoMiembro->email = $value['email'];
            $nuevoMiembro->nombre = $value['name_f'];
            $nuevoMiembro->apellidos = $value['name_l'];
            $nuevoMiembro->calle = $value['street'];
            $nuevoMiembro->calle_2 = $value['street2'];
            $nuevoMiembro->ciudad = $value['city'];
            $nuevoMiembro->estado = $value['state'];
            $nuevoMiembro->codigo_postal = $value['zip'];
            $nuevoMiembro->telefono = $value['phone'];
            $nuevoMiembro->fecha_de_ingreso = $value['added'];
            $nuevoMiembro->status = $value['status'];
            $nuevoMiembro->aprovado = $value['is_approved'];
            $nuevoMiembro->colonia = $value['colonia_cliente'];
            $nuevoMiembro->delegacion = $value['deleg_municipio_cliente'];
            $nuevoMiembro->establecimiento = $value['establecimiento_cliente'];
            $nuevoMiembro->apellido_paterno = $value['apellido_paterno_cliente'];
            $nuevoMiembro->apellido_materno = $value['apellido_materno_cliente'];
            $nuevoMiembro->save();

        }

    }

    public function getDetalleMiembro($id){
        $miembro = [];
        $i = 0;
        $o = 0;
        $e=0;
        $ad = "";
        $intancia = [];
        $something = [];
        try {
            $miembro['info_personal'] = Miembro::where('user_id',$id)->get();
            $miembro['factura'] = DetalleFactura::where('user_id',$id)->get();
            $factura =  DetalleFactura::where('user_id',$id)->get();
           foreach ($factura as $k => $va){
               $miembro ['factura'][$k]['itemFactura'] = ItemFactura::where('factura_id',$va['factura_id'])->get();
               $miembro ['factura'][$k]['facturaPagada']= Facturacion::where('factuar_id',$va['factura_id'])->get();
               $miembro['factura'][$k]['instancia'] = Instancia::where('user_id', $id)->get();
               $alg = Instancia::where('user_id', $id)->get();
               foreach($alg as $ke => $value){
                $something = InstanciaDetalle::where('instancia_id', $value['id'])->get();
                foreach($something as $so){
                    $instancia = $so;
                    
                }
                $miembro['factura'][$ke]['instanciaDetalle']  = $instancia;  
                
               }
                
           }
           // dd($miembro);
            return $miembro;

        }catch (\Exception $exception){
            return  $exception->getMessage();
        }
    }
}
