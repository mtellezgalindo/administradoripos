<?php


namespace App\Functional\ApiAws;


interface IApiConnectAws
{

    public function getDetailInfo($id, $idFacturaPub);
}
