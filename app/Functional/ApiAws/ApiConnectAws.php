<?php


namespace App\Functional\ApiAws;


use App\Functional\ApiAws\Service\LaunchApiAws;

class ApiConnectAws implements IApiConnectAws
{

    use LaunchApiAws;
}
