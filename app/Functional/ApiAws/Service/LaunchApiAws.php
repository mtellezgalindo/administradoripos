<?php


namespace App\Functional\ApiAws\Service;


use App\Instancia;
use App\InstanciaDetalle;
use Illuminate\Support\Facades\Http;

trait LaunchApiAws
{
    public function getDetailInfo($id,$idFacturaPub){
       
        

            $response = Http::get(env('APICONNECT') . '/crear_instancia');
            sleep(15);
            $createInstancia = new Instancia();
            $createInstancia->instancia_id = json_decode($response);
            $createInstancia->user_id = $id;
            $createInstancia->factura_publico_id = $idFacturaPub;
            $createInstancia->save();
            $s = Instancia::where('factura_publico_id', $idFacturaPub)->get();

            foreach ($s as $value) {
                $instancia = $value['instancia_id'];
                $instanciaId = $value['id'];
                $detalleInstancia = Http::get(env('APICONNECT') . '/detalle_instancia/' . $instancia);
                $respuestaDetalle = $detalleInstancia->json();
                $imageID = $respuestaDetalle['Reservations'][0]['Instances'][0]['ImageId'];
                $dnsName = $respuestaDetalle['Reservations'][0]['Instances'][0]['PublicDnsName'];
                sleep(10);
                $createDominio = Http::get(env('APICONNECT') . '/crear_dominio/' . $dnsName);
                 $respuestaDominio = json_decode($createDominio) . '.ivanapps.com';
                
                 $statusDominio = Http::get(env('APICONNECT') . '/status_instancia/' . $instancia);
                 sleep(60);
                 $conectar= Http::get(env('APICONNECT') . '/conexion_server/' . $respuestaDominio);
                 sleep(4);
                 $descargar= Http::get(env('APICONNECT') . '/descargar/' . $respuestaDominio);
                $escribir= Http::get(env('APICONNECT') . '/escribir/' . $respuestaDominio);
                  $actualizar= Http::get(env('APICONNECT') . '/actualizar/' . $respuestaDominio);
                  $actualizar= Http::get(env('APICONNECT') . '/actualizar-config/' . $respuestaDominio);
                  $descargarI= Http::get(env('APICONNECT') . '/descargar-index/' . $respuestaDominio);
                  $escribirI= Http::get(env('APICONNECT') . '/escribir-index/' . $respuestaDominio);
                  $actualizarI= Http::get(env('APICONNECT') . '/actualizar-index/' . $respuestaDominio);
                  $actualizarI= Http::get(env('APICONNECT') . '/actualizar-file-index/' . $respuestaDominio);
                  $descargarC= Http::get(env('APICONNECT') . '/descargar-conf/' . $respuestaDominio);
                  $escribirC= Http::get(env('APICONNECT') . '/escribir-conf/' . $respuestaDominio);
                  $escribirC= Http::get(env('APICONNECT') . '/crear-carpeta/' . $respuestaDominio);
                  $actualizarC= Http::get(env('APICONNECT') . '/actualizar-conf/' . $respuestaDominio);
                  $utlmio= Http::get(env('APICONNECT') . '/ultimos-comandos/' . $respuestaDominio);
                  $utlmo= Http::get(env('APICONNECT') . '/cerrando/' . $respuestaDominio);
                  //$utlmos= Http::get(env('APICONNECT') . '/cerbot/' . $respuestaDominio);

                
                 $zona = $statusDominio["InstanceStatuses"][0]['AvailabilityZone'];
                 $status = $statusDominio["InstanceStatuses"][0]['InstanceStatus']['Status'];

                 $responseIn = $this->saveInstancia($instanciaId, $respuestaDominio, $zona, $status, $dnsName, $imageID);
                 if (!$responseIn) {
                     echo 'no se guard';
                 }

            }
            return true;
       
    }

    public function saveInstancia($idInstaciaR, $url, $zona, $status, $dns, $image){

            $saveInstancia = new InstanciaDetalle();
            $saveInstancia->instancia_id =$idInstaciaR;
            $saveInstancia->url = $url;
            $saveInstancia->disponibilidad_zona =$zona;
            $saveInstancia->estatus_instancia = $status;
            $saveInstancia->dns_name= $dns;
            $saveInstancia->imagen_id = $image;
            $saveInstancia->save();
            return $saveInstancia;


    }

}
