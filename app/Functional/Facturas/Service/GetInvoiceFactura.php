<?php


namespace App\Functional\Facturas\Service;


use App\Facturacion;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;

trait GetInvoiceFactura
{
    public function getFacturasPagadas(){
        $facturasUsuarios = Http::get(env('AMBIENTE_AMEMBER_SANDBOX')."invoice-payments?_key=".env('API_KEYMEMBER')."");
        //dd($facturasUsuarios);
        try {
            DB::table('pagos_miembros')->truncate();
            $this->saveFacturas($facturasUsuarios->json());
            return true;
        }catch (\Exception $exception){
            return  $exception->getMessage();
        }


    }

    public function getFacturas(){
        $facturasUsuarios = Http::get(env('AMBIENTE_AMEMBER_SANDBOX')."invoice?_key=".env('API_KEYMEMBER')."");
        //dd($facturasUsuarios);
        try {
            DB::table('pagos_miembros')->truncate();
            $this->saveFacutasGe($facturasUsuarios->json());
            return true;
        }catch (\Exception $exception){
            return  $exception->getMessage();
        }
    }


    public function saveFacutasGe($fact){
        foreach (array_slice($fact,1) as $key => $value){
            dd($value);
        }
    }

    public function saveFacturas($listadoFacturas){

        foreach (array_slice($listadoFacturas,1) as $key => $value){
            $nuevaFactura = new Facturacion();
            $nuevaFactura->user_id = $value['user_id'];
            $nuevaFactura->id_pago_factura = $value['invoice_payment_id'];
            $nuevaFactura->factuar_id = $value['invoice_id'];
            $nuevaFactura->factura_id_publico = $value['invoice_public_id'];
            $nuevaFactura->metodo_de_pago = $value['paysys_id'];
            $nuevaFactura->id_recibo = $value['receipt_id'];
            $nuevaFactura->id_transaccion = $value['transaction_id'];
            $nuevaFactura->fecha_transaccion = $value['dattm'];
            $nuevaFactura->tipo_moneda = $value['currency'];
            $nuevaFactura->cantidad = $value['amount'];
            $nuevaFactura->discuento = $value['discount'];
            $nuevaFactura->impuesto = $value['tax'];
            $nuevaFactura->envio = $value['shipping'];
            $nuevaFactura->id_display_factura = $value['display_invoice_id'];
            $nuevaFactura->save();

        }

    }

    public function getInformacionFacturaUsuario($id){

        try {
            $factura = Facturacion::where('user_id', $id)->get();
            return $factura;
        }catch (\Exception $exception){
            return  $exception->getMessage();
        }
    }
}
