<?php


namespace App\Functional\Facturas;


interface IFactura
{
    public function getFacturas();
    public function getFacturasPagadas();

}
