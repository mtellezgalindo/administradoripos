<?php


namespace App\Functional\Facturas;


use App\Functional\Facturas\Service\GetFactura;
use App\Functional\Facturas\Service\GetInvoiceFactura;

class Factura implements IFactura
{
    use GetFactura, GetInvoiceFactura;
}
