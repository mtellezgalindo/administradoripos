<?php


namespace App\Functional\ElementosFactura;


interface IDetalleFactura
{
    public function getDetailInfo();
    public function getFacturacionUser($id);
}
