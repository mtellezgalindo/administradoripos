<?php


namespace App\Functional\ElementosFactura;


use App\Functional\ElementosFactura\Service\GetDetalleFactura;

class DetalleFactura implements IDetalleFactura
{
    use GetDetalleFactura;
}
