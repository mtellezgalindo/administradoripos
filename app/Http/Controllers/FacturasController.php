<?php

namespace App\Http\Controllers;

use App\Functional\Facturas\IFactura;
use Illuminate\Http\Request;

class FacturasController extends Controller
{
    //
    private  $factura;
    public function __construct(IFactura $factura)
    {
        $this->factura = $factura;
    }

    public function getFacturas(){
        return $this->factura->getFacturas();

    }
    public function getFacturasPagadas(){
        return $this->factura->getFacturasPagadas();
    }
}
