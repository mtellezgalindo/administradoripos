<?php

namespace App\Http\Controllers;

use App\Functional\ElementosFactura\IDetalleFactura;
use App\Functional\Facturas\IFactura;
use App\Functional\Miembro\IMiembro;
use Illuminate\Http\Request;


class MiembroController extends Controller
{
    //
    private  $member;
    private  $factura;
    private $detallFactura;
    public function __construct(IMiembro $member, IFactura $factura, IDetalleFactura $detalleFactura)    {
        $this->member = $member;
        $this->factura = $factura;
        $this->detallFactura = $detalleFactura;
    }

    /** Agregar un random_string al id de usuario y a la tabla de usuarios
     *
     */
    public function getUsers(){
        $members = $this->member->getallMembers();

        return view('listado-usuarios')->with('usuarios', $members);

    }

    public function  getDetalleUsuario($id){
        $detalleUsuarios = $this->member->getDetalleMiembro($id);
        //dd($detalleUsuarios);
        return view('usuario-perfil')->with('detalles',$detalleUsuarios);
    }
}
