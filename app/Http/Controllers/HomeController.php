<?php

namespace App\Http\Controllers;

use App\Facturacion;
use App\Functional\Facturas\IFactura;
use App\Functional\Miembro\IMiembro;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class HomeController extends Controller
{

    private  $miembros;
    private  $factura;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(IMiembro $miembros, IFactura $factura)
    {
        $this->middleware('auth');
        $this->miembros = $miembros;
        $this->factura = $factura;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $members = $this->miembros->getallMembers();
        $facturas = $this->miembros->getAllInvoice();

        $ganancia = Facturacion::get();
        $sumaGanancia = 0;
        foreach ($ganancia as $item){
            $sumaGanancia += $item->cantidad;
        }
        $totalGanancias = number_format($sumaGanancia,2);

        return view('home')->with('miembros', $members)->with('ganancia', $totalGanancias);
    }

}
