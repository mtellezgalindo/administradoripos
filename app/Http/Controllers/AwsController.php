<?php

namespace App\Http\Controllers;

use App\Functional\ApiAws\IApiConnectAws;
use App\Functional\Miembro\IMiembro;
use Aws\Credentials\Credentials;
use Aws\Ec2\Ec2Client;
use Aws\Sts\StsClient;
use Illuminate\Http\Request;
use Aws\S3\S3Client;
use Aws\Credentials\CredentialProvider;
use Aws\Sdk;



class AwsController extends Controller
{
    //
    private  $member;
    private $api;
    public function __construct(IApiConnectAws $api, IMiembro $member)
    {
        $this->member = $member;
        $this->api = $api;

    }

    public function capsulaCreate($id, $idFacturaPub){
         if($this->api->getDetailInfo($id, $idFacturaPub) == true){
             $detalleUsuarios = $this->member->getDetalleMiembro($id);
             return redirect('detalle-usuario/'.$id);
         }
         return 'error';

    }

}
