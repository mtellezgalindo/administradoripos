<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstanciaDetalle extends Model
{
    //
    protected $table ='instancia_detalle';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $id = 'id';
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    protected $fillable = ['id','instancia_id','url','disponibilidad_zona','estatus_sistema','dns_name','image_id','create_at','updated_at'];
}
