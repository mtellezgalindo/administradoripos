<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Miembro extends Model
{
    //
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'miembros';
    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $id = 'id';
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    protected $fillable = ['id','user_id','email','nombre','apellidos','calle','calle_2','ciudad','estado',
        'codigo_postal','telefono','fecha_de_ingreso','status','aprovado','colonia','delegacion','establecimiento',
        'apellido_paterno','apellido_materno','create_at','updated_at'];



}
