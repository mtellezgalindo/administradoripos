<?php

namespace App\Providers;

use App\Functional\ApiAws\ApiConnectAws;
use App\Functional\ApiAws\IApiConnectAws;
use App\Functional\ElementosFactura\DetalleFactura;
use App\Functional\ElementosFactura\IDetalleFactura;
use App\Functional\Facturas\Factura;
use App\Functional\Facturas\IFactura;
use App\Functional\Facturas\Service\GetFactura;
use App\Functional\Miembro\IMiembro;
use App\Functional\Miembro\Miembro;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->bind(IMiembro::class,Miembro::class);
        $this->app->bind(IFactura::class,Factura::class);
        $this->app->bind(IDetalleFactura::class,DetalleFactura::class);
        $this->app->bind(IApiConnectAws::class,ApiConnectAws::class);

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
