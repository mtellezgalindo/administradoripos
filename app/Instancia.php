<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Instancia extends Model
{
    //

    protected $table ='instancia';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    public $id = 'id';
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    protected $fillable = ['id','instancia_id','user_id','factura_publico_id','create_at','updated_at'];
}
