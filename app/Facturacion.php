<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Facturacion extends Model
{
    //
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pagos_miembros';
    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $id = 'id';
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    protected $fillable = ['id','user_id','id_pago_factura','factuar_id','factura_id_publico','metodo_pago','id_recibo','id_transaccion','fecha_transaccion','tipo_moneda',
        'cantidad','discuento','impuesto','id_display_factura','create_at','updated_at'];
}
