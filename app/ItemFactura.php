<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemFactura extends Model
{
    //
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'items_pago';
    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $id = 'id';
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    protected $fillable = ['id','user_id','precio_de_origen','segundo_precio_origen','elemento_factura_id','factura_id','factura_publico_id','elemento_id','elemento_tipo','elemento_titulo','elemento_descripcion',
        'cantidad','precio','primer_periodo','tipo_moneda','plan_de_pago_id','create_at','updated_at'];



}
